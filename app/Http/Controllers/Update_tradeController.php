<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Update_tradeController extends Controller
{
    function update(){

        //lấy tỉ giá vnd/usd
        function get_currency($from_Currency, $to_Currency, $amount) {
            $amount = urlencode($amount);
            $from_Currency = urlencode($from_Currency);
            $to_Currency = urlencode($to_Currency);
            $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";
            $ch = curl_init();
            $timeout = 0;
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($ch, CURLOPT_USERAGENT,
                "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $rawdata = curl_exec($ch);
            curl_close($ch);
            $data = explode('bld>', $rawdata);
            $data = explode($to_Currency, $data[1]);
            return round($data[0], 2);
        }
       // currency converted
       $vnd_usd = get_currency('USD', 'VND', 1);

        // Get btc data
        $bitstampContent = file_get_contents("https://www.bitstamp.net/api/v2/ticker/btcusd");
        $bitstampInfo = json_decode($bitstampContent);
        $btc_bitstamp = $bitstampInfo->last;

        $bitfinexContent = file_get_contents("https://api.bitfinex.com/v1/pubticker/btcusd");
        $bitfinexInfo = json_decode($bitfinexContent);
        $btc_bitfinex = $bitfinexInfo->last_price;
       $btc_okePrice = max([$btc_bitfinex, $btc_bitstamp]) - rand(1, 2);

        //số tiền vnd cho 1 bit
        $total_btc = ceil($vnd_usd * $btc_okePrice);
		
        
        //lấy tất cả các giao dịch gần nhất 30 ngày
        $list_trades_success = DB::table('trades')
                               ->where('status',5)
                               ->whereBetween('created_at',[Carbon::now()->subDays(29), Carbon::now()])
                               ->get();

        
        foreach($list_trades_success as $items)
        {
           $offer = DB::table('offers')->where('id',$items->offer_id)->first();
           $items->user_id = $offer->user_id;
        }


        $cost_per_unit = 0;
        foreach($list_trades_success as $row)
        {
            $cost_per_unit = $cost_per_unit + ceil($row->total_cost) / $total_btc;

             DB::table('users')
                ->where('id',$row->user_id)
                ->update(['trade_month'=> $cost_per_unit]);
        }


    }
}
